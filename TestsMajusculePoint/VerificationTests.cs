﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MajusculePoint;

namespace TestsMajusculePoint
{
    [TestClass]
    public class VerificationTests
    {
        [TestMethod]
        public void TestMajuscule()
        {
            Assert.AreEqual(true, Program.Majuscule("Plop"));
            Assert.AreEqual(false, Program.Majuscule("plop"));
            Assert.AreEqual(true, Program.Majuscule("Cette phrase debute par une majuscule..."));
        }
        [TestMethod]
        public void TestPoint()
        {
            Assert.AreEqual(true, Program.Point("plop."));
            Assert.AreEqual(false, Program.Point("plop"));
            Assert.AreEqual(true, Program.Point("Cette phtase comporte un point."));
        }
    }
}
