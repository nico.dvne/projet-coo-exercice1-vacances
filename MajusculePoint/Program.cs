﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MajusculePoint
{
    public class Program
    {
        public static bool Majuscule(string ma_phrase)
        {
            bool resultat;
            char premiere_lettre = ma_phrase[0];
            if (premiere_lettre >= 'A' && premiere_lettre <= 'Z')
            {
                Console.WriteLine("Cette phrase commence par une majuscule ");
                resultat = true;
            }
            else
            {
                Console.WriteLine("Cette phrase ne commence pas par une majuscsule ");
                resultat = false;
            }
            return resultat;
            
        }
        public static bool Point(string ma_phrase)
        {
            bool resultat;
            char derniere_lettre = ma_phrase[ma_phrase.Length - 1];
            if (derniere_lettre == '.')
            {
                Console.WriteLine("Cette phrase se termine par un point.");
                resultat = true;
            }
            else
            {
                Console.WriteLine("Cette phrase ne se termine pas par un point. ");
                resultat = false;
            }
            return resultat;
        }
       
              
        static void Main(string[] args)
        {
            string phrase_saisie_utilisateur;
            Console.WriteLine("Veuillez saisir une phrase svp : ");
            phrase_saisie_utilisateur = Console.ReadLine();
            Console.WriteLine("Vous avez saisi : ");
            Console.WriteLine(phrase_saisie_utilisateur);
            Console.WriteLine();
            Majuscule(phrase_saisie_utilisateur);
            Console.WriteLine("");
            Point(phrase_saisie_utilisateur);

            Console.ReadKey();
        }
    }
}
